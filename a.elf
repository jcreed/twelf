% Syntax

tm : type. % worlds, frames
%name tm T.
p : type. % positive props
%name p P.
o : type. % negative props
%name o N.

% Propositions
imp : p -> o -> o.
top : o.
and : o -> o -> o.
all : (tm -> o) -> o.
s- : tm -> o.
s+ : tm -> p.
↓ : o -> p.

% Stub term to make coverage nontrivial
tm/ : tm.

% Judgments
j : type. %name j J.
/lfoc : o -> tm -> j.
/rinv : o -> j.
/rfoc : p -> j.
pf : j -> type. %name pf PF.
%abbrev lfoc = [A][T] pf (/lfoc A T).
%abbrev rinv = [A] pf (/rinv A).
%abbrev rfoc = [B] pf (/rfoc B).

hyp : p -> type. %name hyp H.

% Inference Rules
s+R : hyp (s+ T) -> rfoc (s+ T).
s-L : lfoc (s- T) T.
↓R : rinv A -> rfoc (↓ A).
↓L : lfoc A T -> (hyp (↓ A) -> rinv (s- T)).
topR : rinv top.
andL1 : lfoc (and A B) T <- lfoc A T.
andL2 : lfoc (and A B) T <- lfoc B T.
andR : rinv (and A B) <- rinv A <- rinv B.
impR : rinv (imp B A) <- (hyp B -> rinv A).
impL : lfoc (imp B A) T <- rfoc B <- lfoc A T.
allR : rinv (all A) <- ({x} rinv (A x)).
allL : {t} lfoc (A t) T -> lfoc (all A) T.

do : type. % dest language prop
%name do A.
dtm : type. % dest language term
%name dtm DTM.
fr : type. % frame

at+ : tm -> dtm.
at- : fr -> tm -> dtm.


⇒ : do -> do -> do.  %infix right 10 ⇒.
∧ : do -> do -> do.  %infix right 11 ∧.
∨ : do -> do -> do.  %infix right 10 ∨.
⊤ : do.
⊥ : do.
∃ : (tm -> do) -> do.
∀ : (tm -> do) -> do.
ν : (fr -> do) -> do.
atm : dtm -> do.

xna : o -> do -> type. %name xna XNA.
xns : o -> (fr -> do) -> type. %name xns XNS.
xpa : p -> do -> type. %name xpa XPA.
xps : p -> do -> type. %name xps XPS.

% translate negatives synchronous
xns/imp : xns (imp P N) ([f] P' ∧ (N' f))
           <- xps P P'
           <- xns N N'.
xns/and : xns (and N1 N2) ([f] N1' f  ∨ N2' f)
           <- xns N1 N1'
           <- xns N2 N2'.
xns/top : xns top ([f] ⊥).
xns/all : xns (all N) ([f] ∃ [x] N' x f)
           <- {x} xns (N x) ([f] N' x f).


xns/atm : xns (s- T) ([f] atm (at- f T)).

% translate negatives asynchronous
xna/imp : xna (imp P N) (P' ⇒ N')
           <- xpa P P'
           <- xna N N'.
xna/and : xna (and N1 N2) (N1' ∧ N2')
           <- xna N1 N1'
           <- xna N2 N2'.
xna/top : xna top ⊤.
xna/all : xna (all N) (∀ N')
           <- {x} xna (N x) (N' x).

xna/atm : xna (s- T) (ν [f] atm (at- f T)).

% translate positives synchronous
xps/↓ : xps (↓ N) Z <- xna N Z.
xps/atm : xps (s+ T) (atm (at+ T)).

% translate positives asynchronous
xpa/↓ : xpa (↓ N) (ν Z) 
         <- xns N Z.
xpa/atm : xpa (s+ T) (atm (at+ T)).

dj : type.
/# : fr -> dj.
/conc : do -> dj.
dpf : dj -> type. %name dpf DPF.
%abbrev # = [F] dpf (/# F).
%abbrev conc = [A] dpf (/conc A).

dhyp : do -> type. %name dhyp DH.

νL : (dhyp (ν A) -> # F)
      <- conc (A F).
νR: conc (ν A)
     <- ({f} dhyp (A f) -> # f).

init : dhyp (atm X) -> conc (atm X).


⊤R : conc ⊤.
⊥L : dhyp ⊥ -> dpf J.

% I need this to trick the coverage checker into splitting on the
% derivation in the soundness proof.
void : type.
⊥R0 : void -> conc ⊥.

∧L1 : (dhyp (A ∧ B) -> dpf J)
       <- (dhyp A -> dpf J).
∧L2 : (dhyp (A ∧ B) -> dpf J)
       <- (dhyp B -> dpf J).
∧R : conc (A ∧ B) <- conc A <- conc B.
∨L : (dhyp (A ∨ B) -> dpf J)
      <- (dhyp A -> dpf J)
       <- (dhyp B -> dpf J).
∨R1 : {B} conc A -> conc (A ∨ B).
∨R2 : {A} conc B -> conc (A ∨ B).
⇒R : conc (A ⇒ B) <- (dhyp A -> conc B).
⇒L : (dhyp (A ⇒ B) -> dpf J)
      <- conc A <- (dhyp B -> dpf J).
∃R : {T} conc (A T) -> conc (∃ A) .
∃L : (dhyp (∃ A) -> dpf J) 
      <- ({x} dhyp (A x) -> dpf J).
∀R : conc (∀ A) <- ({x} conc (A x)).
∀L : (dhyp (∀ A) -> dpf J) 
      <- (dhyp (A T) -> dpf J).

